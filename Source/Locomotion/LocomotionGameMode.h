// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LocomotionGameMode.generated.h"

UCLASS(minimalapi)
class ALocomotionGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ALocomotionGameMode();
};



