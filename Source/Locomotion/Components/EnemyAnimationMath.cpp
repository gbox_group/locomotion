// Fill out your copyright notice in the Description page of Project Settings.

#include "EnemyAnimationMath.h"

#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UEnemyAnimationMath::UEnemyAnimationMath()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}

// Called when the game starts
void UEnemyAnimationMath::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner();
	BaseCharacter = Cast<AActor>(UGameplayStatics::GetPlayerController(this, 0));
	CharacterCamera = Cast<UCameraComponent>(BaseCharacter->GetComponentByClass(UCameraComponent::StaticClass()));
	BaseSkMesh = Cast<USkeletalMeshComponent>(Owner->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
}

FRotator UEnemyAnimationMath::GetBaseRotator()
{
	FRotator BaseRotator = FRotator::ZeroRotator;
	if (!Owner || !BaseCharacter)
		return BaseRotator;

	//FVector TargetLocation = CharacterCamera->GetComponentLocation() + CharacterCamera->GetForwardVector() * -200.0f;
	FVector TargetLocation = BaseCharacter->GetActorLocation();
	FVector StartLocation = Owner->GetActorLocation();
	FRotator FindRotator = UKismetMathLibrary::FindLookAtRotation(StartLocation, TargetLocation);
	BaseRotator = UKismetMathLibrary::NormalizedDeltaRotator(Owner->GetActorRotation(), FindRotator);

	return BaseRotator;
}
