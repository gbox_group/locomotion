// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"

#include "EnemyAnimationMath.generated.h"

class UCameraComponent;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class LOCOMOTION_API UEnemyAnimationMath : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UEnemyAnimationMath();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	UFUNCTION()
	FRotator GetBaseRotator();

private:
	UPROPERTY()
	AActor* Owner;

	UPROPERTY()
	AActor* BaseCharacter;

	UPROPERTY()
	UCameraComponent* CharacterCamera;

	UPROPERTY()
	USkeletalMeshComponent* BaseSkMesh;
};
