// Copyright Epic Games, Inc. All Rights Reserved.

#include "LocomotionGameMode.h"
#include "LocomotionCharacter.h"
#include "UObject/ConstructorHelpers.h"

ALocomotionGameMode::ALocomotionGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
